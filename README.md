# Create Redux

## Redux 的使用过程

```js
// statement a reducer (alias stateChanger)
function reducer(state, action) {}

const store = createStore(reducer);

store.subscribe( () => renderApp(store.getState()));

renderApp(store.getState());

store.dispatch(anything)

```
