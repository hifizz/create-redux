/**
 * 本文件主要为 context 的用法
 */

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

const StateLessButton = ({children}, context) =>
  <button style={{background: context.color}}>
    {children}
  </button>;

StateLessButton.contextTypes = {color: PropTypes.string};

class Button extends React.Component {
  render() {
    return (
      <button style={{ background: this.context.color }}>
        {this.props.children}
      </button>
    );
  }
}

Button.contextTypes = {
  color: PropTypes.string
}

class Message extends React.Component {
  render() {
    return (
      <div>
        {this.props.text} <Button color={this.props.color}>Delete</Button>
        <StateLessButton color={this.props.color}>add</StateLessButton>
      </div>
    );
  }
}

class MessageList extends React.Component {
  getChildContext() {
    return { color: "blue" }
  }

  render() {
    const children = this.props.messages.map((message) =>
      <Message text={message.text}/>
    );
    return <div>{children}</div>;
  }
}

MessageList.childContextTypes = {
  color: PropTypes.string
}

ReactDOM.render(
  <MessageList
    messages={[
      {text: "hello"},
      {text: "world"}
    ]}
  />,
  document.getElementById('root')
)
