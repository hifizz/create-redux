import {
  createStore
} from './store'

const appState = {
  title: {
    text: "React.js 小书",
    color: "red"
  },
  content: {
    text: "React.js 小书内容",
    color: "green"
  }
};

function stateChanger(state, action) {
  if(!state) {
    state = appState
  }
  switch (action.type) {
    case "UPDATE_TITLE_TEXT":
      return {
        ...state,
        title: {
          ...state.title,
          text: action.text
        }
      }
    case "UPDATE_TITLE_COLOR":
      return {
        ...state,
        title: {
          ...state.title,
          color: action.color
        }
      }
    default:
      return state;
  }
}

const store = createStore(stateChanger);
let oldState = store.getState();
store.subscribe(() => {
  const newState = store.getState();
  renderApp(newState, oldState);
  oldState = newState;
})

function renderApp(newAppState, oldAppState = {}) {
  if (newAppState === oldAppState) {
    return;
  }
  renderTitle(newAppState.title, oldAppState.title);
  renderContent(newAppState.content, oldAppState.content);
}

function renderTitle(newTitle, oldTitle = {}) {
  if (newTitle === oldTitle) {
    return;
  }
  console.log('render title');
  const titleDOM = document.getElementById('title');
  titleDOM.innerHTML = newTitle.text;
  titleDOM.style.color = newTitle.color;
}

function renderContent(newContent, oldContent) {
  if (newContent === oldContent) {
    return;
  }
  console.log('render content');
  const contentDOM = document.getElementById('content');
  contentDOM.innerHTML = newContent.text;
  contentDOM.style.color = newContent.color;
}

renderApp(store.getState()); // 首次渲染


store.dispatch({
  type: "UPDATE_TITLE_TEXT",
  text: "《REACT 秘籍》"
})
store.dispatch({
  type: "UPDATE_TITLE_COLOR",
  color: "blue"
})

const btn = document.getElementById("btn");
btn.addEventListener("click", function () {
  store.dispatch({
    type: "UPDATE_TITLE_TEXT",
    text: new Date().getTime()
  })
}, false)
