/**
 *
 * @param {object} state 程序状态
 * @param {object} stateChanger
 */
export function createStore(reducer) {
  let state = null;
  const listeners = [];
  const subscribe = (listener) => listeners.push(listener);
  const getState = () => state;
  const dispatch = (action) => {
    state = reducer(state, action);
    listeners.forEach( (listener) => {
      listener()
    })
  }
  dispatch({});
  return { getState, dispatch, subscribe };
}

