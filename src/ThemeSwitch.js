import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from './react-redux/connect';

class ThemeSwitch extends Component {

  static propTypes = {
    themeColor: PropTypes.string
  }

  handleSwitchColor(color) {
    const {onSwitchColor} = this.props;
    onSwitchColor(color)
  }

  render () {
    return (
      <div>
        <button style={{color: this.props.themeColor}} onClick={() => {
          this.handleSwitchColor('red');
        }} >Red</button>
        <button style={{color: this.props.themeColor}} onClick={() => {
          this.handleSwitchColor('blue');
        }}>blue</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    themeColor: state.themeColor
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSwitchColor: (color) => {
      dispatch({
        type: "CHANGE_COLOR",
        themeColor: color
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ThemeSwitch);
